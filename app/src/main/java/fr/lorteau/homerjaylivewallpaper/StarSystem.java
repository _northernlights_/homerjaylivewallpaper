/*
 * Copyright Clement Lorteau (c) 2015
 * This file is part of Homey Jay Live Wallpaper
 */

package fr.lorteau.homerjaylivewallpaper;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.Random;

public class StarSystem
{
    private Bitmap bmp;
    private Star stars[];
    private int numStars;
    private int starMinSize = 10;
    private int starMaxSize = 70;
    private int canvasWidth = 0;
    private int canvasHeight = 0;
    private int animationDurationInFrames = 10;

    public StarSystem(Context context, int width, int height)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        numStars = Integer.parseInt(sharedPref.getString("starsystem_howmany", "50"));
        if (BuildConfig.DEBUG) Log.i(HJWallpaperService.serviceName, "numStars = " + numStars);

        bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.star);
        canvasWidth = width;
        canvasHeight = height;

        stars = new Star[numStars];
        for (int i = 0; i < stars.length; i++)
        {
            int x = randInt(1, canvasWidth);
            int y = randInt(1, 3*canvasHeight/4);
            int alpha = 255;
            int size = randInt(starMinSize, starMaxSize);
            int animationFrame = randInt(0, animationDurationInFrames);
            stars[i] = new Star(x, y, size, alpha, animationFrame);
        }
    }

    public void DrawFrame(Canvas canvas)
    {
        if (canvasWidth != 0 && canvasHeight != 0)
        {

            for (Star star : stars)
            {
                if (star.currentAnimationFrame >= animationDurationInFrames)
                    star.currentAnimationFrame = 0;

                Paint p = new Paint();
                p.setAlpha(star.alpha - 10 * star.currentAnimationFrame);

                Rect srcRect = new Rect(0, 0, bmp.getWidth(), bmp.getHeight());
                Rect dstRect = new Rect(star.x,
                        star.y,
                        star.x + star.size,
                        star.y + star.size);
                canvas.drawBitmap(bmp, srcRect, dstRect, p);

                star.currentAnimationFrame++;
            }

        }
    }

    public static int randInt(int min, int max) {
        Random rand = new Random();
        return rand.nextInt((max - min) + 1) + min;
    }

    private class Star
    {
        public int x;
        public int y;
        public int size;
        public int alpha;
        public int currentAnimationFrame;

        public Star(int _x, int _y, int _size, int _alpha, int _cf)
        {
            x = _x;
            y = _y;
            size = _size;
            alpha = _alpha;
            currentAnimationFrame = _cf;
        }
    }
}
