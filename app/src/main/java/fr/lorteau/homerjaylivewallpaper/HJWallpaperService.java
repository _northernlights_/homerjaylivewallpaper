/*
 * Copyright Clement Lorteau (c) 2015
 * This file is part of Homey Jay Live Wallpaper
 */

package fr.lorteau.homerjaylivewallpaper;

import android.content.SharedPreferences;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.SurfaceHolder;

public class HJWallpaperService extends WallpaperService
{
    public static String serviceName = "HJWallpaper";

    @Override
    public WallpaperService.Engine onCreateEngine()
    {
        return new HJWallpaperEngine();
    }

    private class HJWallpaperEngine extends WallpaperService.Engine implements SharedPreferences.OnSharedPreferenceChangeListener {
        private boolean visible;
        private Handler handler;
        private final int frameDuration = 30;
        private Drawer drawer;
        private SurfaceHolder holder;

        public HJWallpaperEngine() {
            handler = new Handler();
        }

        @Override
        public void onCreate(SurfaceHolder surfaceHolder) {
            super.onCreate(surfaceHolder);

            SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            sharedPref.registerOnSharedPreferenceChangeListener(this);

            surfaceHolder.addCallback(new SurfaceHolder.Callback()
            {
                public void surfaceCreated(SurfaceHolder _holder)
                {
                    if (BuildConfig.DEBUG) Log.i(serviceName, "Surface created");
                    holder = _holder;
                    try
                    {
                        drawer = new Drawer(getApplicationContext(), _holder);
                    }
                    catch (Exception e)
                    {
                        if (BuildConfig.DEBUG) Log.e(serviceName, "Could not initialize drawer", e);
                    }
                }

                public void surfaceDestroyed(SurfaceHolder _holder)
                {
                    if (BuildConfig.DEBUG) Log.i(serviceName, "Surface destroyed");
                    drawer = null;
                }

                public void surfaceChanged(SurfaceHolder _holder,
                                           int format, int width,
                                           int height)
                {
                    if (BuildConfig.DEBUG) Log.i(serviceName, "Surface changed");
                }
            });
        }

        private Runnable drawHJ = new Runnable() {
            public void run() {
                draw();
            }
        };


        private void draw() {
            if (visible)
            {
                if (drawer != null)
                {
                    try
                    {
                        drawer.DrawAnimationFrame();
                    }
                    catch (Exception oops)
                    {
                        if (BuildConfig.DEBUG) Log.e(serviceName, "Error when drawing frame", oops);
                    }
                }
                handler.removeCallbacks(drawHJ);
                handler.postDelayed(drawHJ, frameDuration);
            }
            else
            {
                handler.removeCallbacks(drawHJ);
            }
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            this.visible = visible;
            if (visible) {
                handler.post(drawHJ);
            } else {
                handler.removeCallbacks(drawHJ);
            }
        }

        @Override
        public void onDestroy() {
            super.onDestroy();
            handler.removeCallbacks(drawHJ);
        }

        public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key)
        {
            if (drawer != null && holder != null)
            {
                if (BuildConfig.DEBUG) Log.i(serviceName, "Restarting due to preferences change");
                try
                {
                    drawer = new Drawer(getApplicationContext(), holder);
                }
                catch (Exception e)
                {
                    if (BuildConfig.DEBUG) Log.e(serviceName, "Could not initialize drawer", e);
                }
            }
        }
    }
}
