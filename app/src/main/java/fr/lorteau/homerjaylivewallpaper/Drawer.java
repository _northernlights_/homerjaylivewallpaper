/*
 * Copyright Clement Lorteau (c) 2015
 * This file is part of Homey Jay Live Wallpaper
 */

package fr.lorteau.homerjaylivewallpaper;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.SurfaceHolder;

public class Drawer
{
    private SurfaceHolder holder;
    private Bitmap bgbmp;
    private boolean enableShootingStar;
    private ShootingStar shootingStar;
    private StarSystem starSystem;

    public Drawer(Context context, SurfaceHolder h)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        enableShootingStar = sharedPref.getBoolean("enable_shootingstar_checkbox", true);
        if (BuildConfig.DEBUG) Log.i(HJWallpaperService.serviceName, "enableShootingStar = " + enableShootingStar);
        bgbmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.back);
        if (enableShootingStar) shootingStar = new ShootingStar(context);
        Canvas canvas = h.lockCanvas();
        starSystem = new StarSystem(context, canvas.getWidth(), canvas.getHeight());
        h.unlockCanvasAndPost(canvas);

        holder = h;
    }

    //create background, scale, and crop according to screen size (show right of image)
    private void DrawBackground(Canvas canvas)
    {
        int screenwidth = canvas.getWidth();
        int screenheight = canvas.getHeight();
        int imagewidth = bgbmp.getWidth();
        int imageheight = bgbmp.getHeight();

        int cropleft = screenwidth - imagewidth;
        int croptop = screenheight - imageheight;
        canvas.drawBitmap(bgbmp, cropleft, croptop, null);
    }

    public void DrawAnimationFrame()
    {
        Canvas canvas = holder.lockCanvas();
        if(canvas != null)
        {
            DrawBackground(canvas);
            starSystem.DrawFrame(canvas);
            if (enableShootingStar) shootingStar.RequestDrawFrame(canvas);
            holder.unlockCanvasAndPost(canvas);
        }
    }

}
