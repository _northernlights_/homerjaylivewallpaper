/*
 * Copyright Clement Lorteau (c) 2015
 * This file is part of Homey Jay Live Wallpaper
 */

package fr.lorteau.homerjaylivewallpaper;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.preference.PreferenceManager;
import android.util.Log;

public class ShootingStar {
    private Bitmap shootingStarBmp;
    private int shootingStarYVelocity = 22;
    private int shootingStarXVelocity = 150;
    private int shootingStarX = 0;
    private int shootingStarY = 0;
    private boolean shootingStarOnScreen = false;
    private int showShootingStarDelayMS;
    private long shootingStarLastShown = 0;

    public ShootingStar(Context context)
    {
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(context);
        showShootingStarDelayMS = Integer.parseInt(sharedPref.getString("shootingstar_frequency_second", "30000"));
        if (BuildConfig.DEBUG) Log.i(HJWallpaperService.serviceName, "showShootingStarDelayMS = " + showShootingStarDelayMS);
        shootingStarBmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.shootingstar);
    }

    public void RequestDrawFrame(Canvas canvas)
    {
        if (!shootingStarOnScreen &&
                ((System.currentTimeMillis() - shootingStarLastShown) > showShootingStarDelayMS))
        {
            shootingStarLastShown = System.currentTimeMillis();
            DrawShootingStarFrame(canvas);
        }
        else if (shootingStarOnScreen) DrawShootingStarFrame(canvas);
    }

    private void DrawShootingStarFrame(Canvas canvas)
    {
        if (!shootingStarOnScreen)
        {
            //beginning of shooting star animation
            shootingStarY = StarSystem.randInt(50, 2*canvas.getHeight()/3);
            shootingStarX = -shootingStarBmp.getWidth();
        }
        else
        {
            shootingStarX += shootingStarXVelocity;
            shootingStarY -= shootingStarYVelocity;
        }
        if (shootingStarX >= canvas.getWidth())
        {
            //end of animation
            shootingStarOnScreen = false;
        }
        else
        {
            canvas.drawBitmap(shootingStarBmp, shootingStarX, shootingStarY, null);
            shootingStarOnScreen = true;
        }
    }
}
